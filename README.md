# w-suitability

## Framework:
Vue.js (https://vuejs.org/)

## Plugins / bibliotecas:
- Vuetify (https://vuetifyjs.com/) para interface e alguns componentes visuais;
- Vuex (https://vuex.vuejs.org/) gerenciador de estado da aplicação;
- Axios (https://github.com/axios/axios) client para requisições HTTP;
- Vuetyped (https://github.com/Orlandster/vue-typed-js) para efeitos de escrita de texto.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
