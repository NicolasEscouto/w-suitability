import axios from 'axios'

export default {
    postMessage(body = {}) {
        body.context = 'suitability'
        
        return axios.post('conversation/message', body)
    },

    getProfileResult(answers) {
        return axios.post('suitability/finish', { answers })
    }
}