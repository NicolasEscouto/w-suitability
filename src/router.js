import Vue from 'vue'
import Router from 'vue-router'

import Chat from './components/chat/Chat'
import ProfileFinish from './components/ProfileFinish'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            components: { default: Chat }
        },

        {
            path: '/finish',
            components: { default: ProfileFinish }
        }
    ]
})