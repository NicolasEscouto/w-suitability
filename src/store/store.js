import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        messages: [],
        answers: {},
        customerField: {},
        showCustomerField: false,
        responses: [],
        lastQuestionId: ''
    },

    getters: {
        getMessages(state) {
            return state.messages
        },
        getAnswers(state) {
            return state.answers
        },
        getShowCustomerField(state) {
            return state.showCustomerField
        },
        getCustomerFieldType(state) {
            return state.customerField.fieldType
        },
        getCustomerFieldMask(state) {
            return state.customerField.data[0].mask
        },
        getCustomerField(state) {
            return state.customerField
        },
        getResponses(state) {
            return state.responses
        },
        getLastQuestionId(state) {
            return state.lastQuestionId
        }
    },

    mutations: {
        addMessage(state, message) {
            state.messages.push(message)
        },
        addAnswer(state, answer) {
            Object.assign(state.answers, answer)
        },
        setCustomerField(state, data) {
            state.customerField = data
        },
        setShowCustomerField(state, show) {
            state.showCustomerField = show
        },
        setResponses(state, data) {
            state.responses = data
        },
        setLastQuestionId(state, questionId) {
            state.lastQuestionId = questionId
        }
    },

    actions: {
        storeResponseData(context, data) {
            data.messages.forEach(msg => {
                context.commit('addMessage', { type: 'warren', message: msg.value })
            })

            context.commit('setResponses', data.responses)

            context.commit('setLastQuestionId', data.id)

            context.commit('setCustomerField', {})

            if (data.buttons.length) {
                context.commit('setCustomerField', { fieldType: 'buttons', data: [...data.buttons] })
            }

            if (data.inputs.length) {
                context.commit('setCustomerField', { fieldType: 'inputs', data: [...data.inputs] })
            }

            if (data.radios.length) {
                context.commit('setCustomerField', { fieldType: 'radios', data: [...data.radios] })
            }

            if (data.checkbox.length) {
                context.commit('setCustomerField', { fieldType: 'checkbox', data: [...data.checkbox] })
            }
        }
    }
})